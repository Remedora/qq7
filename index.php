<?php

header('Content-Type:text/html;charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages=array();
    $errors = array();
    $values = array();
    if (!empty($_COOKIE['save'])) {
        setcookie('save','',100000);
        setcookie('login', '',100000);
        setcookie('pass', '', 100000);
        $messages['save'] = 'Результаты сохранены.';
        if (!empty($_COOKIE['pass'])) {
            $messages['login_and_password'] = sprintf('Вы можете <a href="login.php">изменить данные с помощью этого </a>логина <strong>%s</strong>
            и пароля <strong>%s</strong>',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['pass']));
          }
    }
    $errors = FALSE;
    $flag=FALSE;
    $superspos_separated='';
    $errors['name'] = !empty($_COOKIE['name_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['date'] = !empty($_COOKIE['date_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['limb'] = !empty($_COOKIE['limb_error']);
    $errors['superspos'] = !empty($_COOKIE['superspos_error']);
    $errors['agree'] = !empty($_COOKIE['agree_error']);
    $errors['biography'] = !empty($_COOKIE['biography_error']);
    if ($errors['name']) {
        if($_COOKIE['name_error']=='none'){
            setcookie('name_error','',100000);
            $messages['name'] = '<div class="error">Ошибка: Вы не ввели имя.</div>';
    }
    if($_COOKIE['name_error']=='Unacceptable symbols'){
            setcookie('name_error','',100000);
            $messages['name'] = '<div class="error">Ошибка: Недопустимые символы в имени:а-я,А-Я,0-9,\ - _ </div>';
        }
    }
    if ($errors['email']) {
        if($_COOKIE['email_error']=='none'){
            setcookie('email_error','',100000);
            $messages['email'] = '<div class="error">Ошибка: Вы не указали почту.</div>';
        }
        if($_COOKIE['email_error']=='invalid address'){
            setcookie('email_error','',100000);
            $messages['email'] = '<div class="error">Ошибка: Почта указана некорректно.Пример:email@yandex.ru</div>';
        }
    }
    if($errors['date']){
        setcookie('date_error','',100000);
        $messages['date'] = '<div class="error">Ошибка: Вы не указали год рождения.</div>';
    }
    if($errors['gender']){
            setcookie('gender_error','',100000);
            $messages['gender'] = '<div class="error">Ошибка: Вы не указали пол.</div>';
    }
    if ($errors['limb']) {
        setcookie('limb_error','',100000);
        $messages['limb'] = '<div class="error">Ошибка: Вы не указали количество конечностей.</div>';
    }
    if($errors['superspos']){
        if($_COOKIE['superspos_error']=="none"){
            setcookie('superspos_error','',100000);
            $messages['superspos'] = '<div class="error">Ошибка: Вы не указали способности.</div>';
        }
        if($_COOKIE['superspos_error']=="noneselected"){
            setcookie('superspos_error','',100000);
            $messages['superspos'] = '<div class="error">Ошибка: Вы не указали способности.</div>';
        }
    }
    if ($errors['biography']) {
            setcookie('biography_error','',100000);
            $messages['biography'] = '<div class="error">Ошибка: Вы не поделились своей биографией.</div>';

    }
    if($errors['agree']){
        setcookie('agree_error','',100000);
        $messages['agree'] = '<div class="error">Ошибка: Вы не подтвердили согласие.</div>';
    }

    $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['date'] = empty($_COOKIE['date_value']) ? '' : strip_tags($_COOKIE['date_value']);
    $values['gender'] = empty($_COOKIE['gender_value']) ? '' : strip_tags($_COOKIE['gender_value']);
    $values['limb'] = empty($_COOKIE['limb_value']) ? '' : strip_tags($_COOKIE['limb_value']);
    $values['superspos'] = empty($_COOKIE['superspos_value']) ? '' : strip_tags($_COOKIE['superspos_value']);
    $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
    $values['agree'] = empty($_COOKIE['agree_value']) ? '' : strip_tags($_COOKIE['agree_value']);
    if (session_start() && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
        $user = 'u20300';
        $password = '5434493';
        $log=$_SESSION['login'];
        $db = new PDO('mysql:host=localhost;dbname=u20300', $user, $password,
        array(PDO::ATTR_PERSISTENT => true));
        try{
        $stmt = $db->prepare("SELECT name,email,date1,gender,limb,superspos,biography1,agree FROM qq WHERE login = '$log'");
        $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_LAZY))
            {
                 $values['name']=$row['name'];
                 $values['email']=$row['email'];
                 $values['date']=$row['date1'];
                 $values['gender']=$row['gender'];
                 $values['limb']=$row['limb'];
                 $values_f=array();
                 if(!empty($row['superspos'])){
                    if(stristr($row['superspos'],'net') == TRUE) {
                      array_push($values_f,'net');
                    }
                    if(stristr($row['superspos'],'unvisibility') == TRUE) {
                      array_push($values_f,'unvisibility');
                    }
                    if(stristr($row['superspos'],'wallhack') == TRUE) {
                      array_push($values_f,'wallhack');
                    }
                    if(stristr($row['superspos'],'mindcontrol') == TRUE) {
                      array_push($values_f,'mindcontrol');
                    }
                    if(stristr($row['superspos'],'changebody') == TRUE) {
                      array_push($values_f,'changebody');
                    }
                    if(stristr($row['superspos'],'changereality') == TRUE) {
                      array_push($values_f,'changereality');
                    }
                    $values['superspos']=serialize($values_f);
                  }
                 $values['biography']=$row['biography1'];
                 $values['agree']=$row['agree'];
            }
    }catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
        } 
    }
    include('form.php');
}
else {
    /*ПРОВЕРКА НА НАЛИЧИЕ ОШИБОК*/
    $errors = FALSE;
            if (empty($_POST['name'])) {
                setcookie('name_error', 'none', time() + 24 * 60 * 60);
                setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else {
                if (!preg_match("#^[aA-zZ0-9\-_]+$#",$_POST['name'])){
                    setcookie('name_error', 'Unacceptable symbols', time() + 24 * 60 * 60);
                    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                    $errors=TRUE;
                }else{
                    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                }
            }
            if (empty($_POST['email'])) {
                setcookie('email_error', 'none', time() + 24 * 60 * 60);
                setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,10})$/i", $_POST['email'])) {
                    setcookie('email_error', 'invalid address', time() + 24 * 60 * 60);
                    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
                    $errors = TRUE;
                }else{
                    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
                }
            }
            if (empty($_POST['date'])) {
                setcookie('date_error', 'none', time() + 24 * 60 * 60);
                setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
            }
            if (empty($_POST['gender'])) {
                setcookie('gender_error', 'none', time() + 24 * 60 * 60);
                setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
            }
            if (empty($_POST['limb'])) {
                setcookie('limb_error', 'none', time() + 24 * 60 * 60);
                setcookie('limb_value', $_POST['limb'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('limb_value', $_POST['limb'], time() + 30 * 24 * 60 * 60);
            }
            if(!isset($_POST['superspos'])){
                setcookie('superspos_error', 'none', time() + 24 * 60 * 60);
                setcookie('superspos_value', serialize($_POST['superspos']), time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                $superspos_mass=$_POST['superspos'];
                $flag=FALSE;
                for($w=0;$w<count($superspos_mass);$w++){
                    if($superspos_mass[$w]=="net"){
                        $flag=TRUE;break;
                    }
                }
                if($flag && count($superspos_mass)!=1){
                    setcookie('superspos_error', 'noneselected', time() + 24 * 60 * 60);
                    setcookie('superspos_value',serialize($_POST['superspos']), time() + 30 * 24 * 60 * 60);
                    $errors = TRUE;
                }else{
                    setcookie('superspos_value',serialize($_POST['superspos']), time() + 30 * 24 * 60 * 60);
                }
            }
            if (empty($_POST['biography'])) {
                setcookie('biography_error', 'none', time() + 24 * 60 * 60);
                setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
            }
            if (empty($_POST['agree'])) {
                setcookie('agree_error', 'none', time() + 24 * 60 * 60);
                setcookie('agree_value', $_POST['agree'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('agree_value', $_POST['agree'], time() + 30 * 24 * 60 * 60);
            }
            if ($errors) {
                header('Location:index.php');
                exit();
            }
            else {
                setcookie('name_error', '', 100000);setcookie('limb_error', '', 100000);
                setcookie('email_error', '', 100000);setcookie('superspos_error', '', 100000);
                setcookie('date_error', '', 100000);setcookie('biography_error', '', 100000);
                setcookie('gender_error', '', 100000);setcookie('agree_error', '', 100000);
            }

        if (session_start() && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) 
        {//перезапись по логину
        $user = 'логин';
        $password = 'пароль';
        $superspos_separated='';
        $log=$_SESSION['login'];
        $db = new PDO('mysql:host=localhost;dbname=u20300', $user, $password,array(PDO::ATTR_PERSISTENT => true));
        try {
        $stmt = $db->prepare("UPDATE qq SET name=?,email=?,date1=?,gender=?,limb=?,superspos=?,biography1=?,agree=? WHERE login='$log' ");
        
        $name=$_POST["name"];
        $email=$_POST["email"];
        $date1=$_POST["date"];
        $gender=$_POST["gender"];
        $limb=$_POST["limb"];
        if(!empty($_POST['superspos'])){
            $superspos_mass=$_POST['superspos'];
            for($w=0;$w<count($superspos_mass);$w++){
                if($flag){
                    if($superspos_mass[$w]!="net")unset($superspos_mass[$w]);
                    $superspos_separated=implode(' ',$superspos_mass);
                }else{
                    $superspos_separated=implode(' ',$superspos_mass);
                }
            }
        }
        $superspos=$superspos_separated;
        $biography1=$_POST["biography"];
        $agree=$_POST["agree"];
        
        $stmt->execute(array($name,$email,$date1,$gender,$limb,$superspos,$biography1,$agree,));
        }catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
    }else 
    {
        $logins_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $pass_chars='0123456789abcdefghijklmnopqrstuvwxyz';
        $login = substr(str_shuffle($logins_chars), 0, 3);
        $password =substr(str_shuffle($pass_chars),0,3);
        setcookie('login', $login);
        setcookie('pass', $password);
        if(!empty($_POST['superspos'])){
            $superspos_mass=$_POST['superspos'];
            for($w=0;$w<count($superspos_mass);$w++){
                if($flag){
                    if($superspos_mass[$w]!="net")unset($superspos_mass[$w]);
                    $superspos_separated=implode(' ',$superspos_mass);
                }else{
                    $superspos_separated=implode(' ',$superspos_mass);
                }
            }
        }
        $user = 'u20300';
        $pass = '5434493';
        $db = new PDO('mysql:host=localhost;dbname=u20300', $user, $pass,
        array(PDO::ATTR_PERSISTENT => true));
        try {
        $stmt = $db->prepare("INSERT INTO qq (name,login,password,email, date1, gender, limb,superspos,biography1,agree) 
        VALUES (:name,:login,:password,:email, :date1, :gender, :limb,:superspos,:biography1, :agree)");
        $stmt->bindParam(':name', $name_db);
        $stmt->bindParam(':login', $login_db);
        $stmt->bindParam(':password', $pass_db);
        $stmt->bindParam(':email', $email_db);
        $stmt->bindParam(':date1', $date_db);
        $stmt->bindParam(':gender', $gender_db);
        $stmt->bindParam(':limb', $limb_db);
        $stmt->bindParam(':superspos', $superspos_db);
        $stmt->bindParam(':biography1', $biography1_db);
        $stmt->bindParam(':agree', $agree_db);
        $name_db=htmlspecialchars($_POST["name"]);
        $login_db=$login;
        $pass_db=md5($password);
        $email_db=htmlspecialchars($_POST["email"]);
        $date_db=htmlspecialchars($_POST["date"]);
        $gender_db=htmlspecialchars($_POST["gender"]);
        $limb_db=htmlspecialchars($_POST["limb"]);
        $superspos_db=$superspos_separated;
        $biography1_db=htmlspecialchars($_POST["biography"]);
        $agree_db=htmlspecialchars($_POST["agree"]);
        $stmt->execute();
        }
        catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
        }
    }
    setcookie('save', '1');
    header('Location:index.php');
}
?>
